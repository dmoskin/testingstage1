    "Success is one percent inspiration, ninety-nine percent perspiration"
                                                            Thomas Edison


**This project was created to learn how to make simple junit test.**

Project has simple structure with build process based on gradle version 4.9.
DO NOT CHANGE BUILD.GRADLE FILE, IT HAS ALL YOU NEED FOR SUCCESSFUL COMPLETION OF THIS TASK. JUST CREATE YOUR OWN PACKAGE AND START WORK.
For simplicity gradle wrapper was added into project(you don't have to install it on your system).
[How to use gradle wrapper](https://www.mkyong.com/gradle/how-to-use-gradle-wrapper/)

## Task
You have to create your own implementation of List data structure, implement all methods and cover each method with
all possible test cases. There is an example in package **com.test.tut**

1. Clone project into your local system
2. Open it with your favorite IDE
3. Create your own package at path **com.test.tut._\<lastName\>_._\<firstName\>_** in **src/main/java** directory
4. Create class in this package  which represents your own implementation of List data structure 
5. After implementing your custom data structure create your own package at path **com.test.tut._\<lastName\>_._\<firstName\>_** in **src/test/java** directory
6. Start to write unit tests for your class methods. Create test cases for methods as much as you can. You can write down all tests in one test class or you can implement tests in different classes. For example, one class contains all tests for one method from your implementation list data structure.
7. Push yor results into repository, write me in Telegram(@DmitryMospanenko (https://t.me/DmitryMospanenko)) to check what you've done.

_Keep your test simple and clear, don't do too much assertions in one test. Try to find best practices for building Junit test._    

## Links
1. [Junit user guide](https://junit.org/junit5/docs/current/user-guide/)
2. [Junit tutorial](http://www.vogella.com/tutorials/JUnit/article.html)
3. [Another junit tutorial](http://www.baeldung.com/junit-5)

## Example
Explore how to create your class and tests. For this task you can check in **com.test.tut.mospanenko.dmytro** package. 

