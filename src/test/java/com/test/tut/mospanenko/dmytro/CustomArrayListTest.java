package com.test.tut.mospanenko.dmytro;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.*;

/**
 * This is test class with using Junit 5
 */
class CustomArrayListTest {

    @Test
    void testDefaultCostumeArrayListCreation() {
        CustomArrayList myList = new CustomArrayList();
        assertThat(myList).isNotNull();
        assertThat(myList).hasSize(10); //check default size of costume List
    }

    @Test
    void testCostumeArrayListCreationWithInitialSize() {
        int initialSize = 25;
        CustomArrayList myList = new CustomArrayList(initialSize);
        assertThat(myList).hasSize(initialSize);
    }

    @Test
    void testCostumeArrayListCreationWithInitialZeroSize() {
        int initialSize = 0;

        assertThat(new ArrayList<>(0)).isEmpty();
        CustomArrayList myList = new CustomArrayList(initialSize);
        assertThat(myList).isEmpty();
    }

    @Test
    void testCostumeArrayListCreationWithNegativeInitialSize() {
        final int initialSize = -5;

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> new CustomArrayList(initialSize))
                .withMessageContaining("can't be negative")
                .withNoCause();
    }


}